<?php
/**
 * Generic often needed helper functions 
 *
 * @author burny
 */
class helper
{

	/**
	 * Similar to the print_r function, except the output is returned as a string. Uses output buffering, so the output is the actual print_r content.
	 * 
	 * @param mixed $var
	 * @return string
	 */
	static function sprint_r($var)
	{
		ob_start();
		print_r($var);
		$output = ob_get_contents();
		ob_end_clean();
		return $output;
	}

	/**
	 * Convert url query string to associative array of variables
	 * 
	 * @param type $string
	 * @return array
	 */
	static function urlQueryToArray($string) {
		$array = array();
		$vars = explode("&", $string);
		foreach ($vars as $var) {
			$keyNval = explode("=", $var);
			$array[$keyNval[0]] = $keyNval[1];
		}
		return $array;
	}
	
	/**
	 * Convert number of seconds to Hours:Mins:Secs
	 * @param int $secs
	 * @return string
	 */
	static function secsToStr($secs)
	{
		if ($secs >= 86400)
		{
			$days = floor($secs / 86400);
			$secs = $secs % 86400;
			$r = $days . ' day';
			if ($days <> 1)
			{
				$r.='s';
			}if ($secs > 0)
			{
				$r.=', ';
			}
		}
		if ($secs >= 3600)
		{
			$hours = floor($secs / 3600);
			$secs = $secs % 3600;
			$r.=$hours . ' hour';
			if ($hours <> 1)
			{
				$r.='s';
			}if ($secs > 0)
			{
				$r.=', ';
			}
		}
		if ($secs >= 60)
		{
			$minutes = floor($secs / 60);
			$secs = $secs % 60;
			$r.=$minutes . ' minute';
			if ($minutes <> 1)
			{
				$r.='s';
			}if ($secs > 0)
			{
				$r.=', ';
			}
		}
		$r.=$secs . ' second';
		if ($secs <> 1)
		{
			$r.='s';
		}
		return $r;
	}

}
